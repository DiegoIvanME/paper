<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
	<id>@app_id@</id>
  <name translatable="no">Paper</name>
  <summary>Note-taking app</summary>
	<description>
	  <p>Create notebooks, take notes in markdown</p>
	  <p>Features:</p>
    <ul>
      <li>Almost WYSIWYG markdown rendering</li>
      <li>Searchable through GNOME search</li>
      <li>Highlight &amp; Strikethrough text formatting</li>
      <li>App recoloring based on notebook color</li>
      <li>Trash can</li>
    </ul>
	</description>

  <launchable type="desktop-id">@app_id@.desktop</launchable>

  <releases>
    <release type="stable" version="22.6" date="2022-06-02">
      <description translatable="no">
	      <p>New Stuff:</p>
        <ul>
          <li>Added more icons for notebooks</li>
          <li>Added ability to drag notes to a notebook, to move them</li>
          <li>Made window state get saved</li>
        </ul>
	      <p>Translations:</p>
        <ul>
          <li>French</li>
          <li>Portuguese (Brazilian)</li>
          <li>Occitan</li>
          <li>Russian</li>
        </ul>
	      <p>UI Tweaks:</p>
        <ul>
          <li>Made headings more compact</li>
          <li>Improved mobile toolbar layout</li>
          <li>Improved contrast for OLED mode</li>
          <li>Made note cards look closer to normal GNOME sidebars</li>
          <li>Removed search from preferences window</li>
          <li>Improved the preferences UI</li>
          <li>Redesigned app icons</li>
        </ul>
	      <p>Fixed:</p>
        <ul>
          <li>Blurry icon in notebook creation popup</li>
          <li>Sidebar folding glitchiness</li>
          <li>Emojis not being properly displayed in notebook icons</li>
          <li>Random bugs with deleting &amp; creating notes</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="22.5" date="2022-05-24">
      <description translatable="no">
	      <p>This update brings the setting to change where notebooks are stored, among other improvements:</p>
        <ul>
          <li>Unobtrusive headerbars</li>
          <li>Search all notes from inside the app</li>
          <li>OLED mode setting</li>
          <li>UI tweaks</li>
          <li>New icon</li>
          <li>Several bug-fixes</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="22.4" date="2022-05-19">
      <description translatable="no">
	      <p>New Features:</p>
        <ul>
          <li>Move to Notebook... option in note context menus</li>
          <li>i18n: Spanish, Catalan, toki pona</li>
          <li>Open Containing Folder option in note context menus</li>
        </ul>
	      <p>UI:</p>
        <ul>
          <li>Actual icons on notebooks</li>
          <li>Move to Notebook... option in note context menus</li>
          <li>i18n: Spanish, Catalan, toki pona</li>
          <li>Open Containing Folder option in note context menus</li>
          <li>Different ways to choose the icon characters</li>
          <li>UI &amp; Markdown rendering tweaks</li>
          <li>Better confirmation dialogs</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="22.3" date="2022-05-04">
      <description translatable="no">
	      <p>Search:</p>
        <ul>
          <li>Gnome search integration</li>
          <li>Fuzzy matching in search</li>
          <li>New icon</li>
          <li>Tweaked UI</li>
          <li>Icon right click menu</li>
        </ul>
	      <p>Graphical:</p>
        <ul>
          <li>New icon</li>
          <li>Tweaked UI</li>
          <li>Icon right click menu</li>
        </ul>
      </description>
    </release>
    <release type="stable" version="22.2" date="2022-04-23"/>
    <release type="stable" version="22.1" date="2022-04-06"/>
  </releases>

  <screenshots>
    <screenshot type="default">
      <image>https://posidon.io/paper/screenshot/22_0602_kule.png</image>
      <caption>Markdown document</caption>
    </screenshot>
  </screenshots>

	<metadata_license>CC0-1.0</metadata_license>
	<project_license>GPL-3.0+</project_license>
  <content_rating type="oars-1.1"/>

  <developer_name translatable="no">Zagura</developer_name>
  <project_group translatable="no">posidon</project_group>

  <url type="homepage">https://posidon.io/paper</url>
  <url type="bugtracker">https://gitlab.com/posidon_software/paper/issues</url>

  <branding>
    <color type="primary" scheme_preference="light">#c6eabf</color>
    <color type="primary" scheme_preference="dark">#246c4d</color>
  </branding>

  <translation type="gettext">@app_id@</translation>
</component>
